package com.lendingpoint.service;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lendingpoint.dao.IDALogDao;
import com.lendingpoint.domain.ApplicationData;
import com.lendingpoint.domain.IDALog;

@Service("idaLogService")
@Transactional(value = "transactionManager")
public class IDALogServiceImpl implements IDALogService {

	@Autowired
	private IDALogDao idaLogDao;
	@Override
	public Long logRequest(ApplicationData appData,String cronJobId) {
		ObjectMapper mapper = new ObjectMapper();
		IDALog idaLog = new IDALog();
		Long logId = null;
		
		try {
			String appDataInstring = mapper.writeValueAsString(appData);
			idaLog.setApplicationId(appData.getAppId());
			idaLog.setApplicationName(appData.getAppName());
			idaLog.setFirstName(appData.getFirstName());
			idaLog.setLastName(appData.getLastName());
			idaLog.setRequestBody(appDataInstring);
			idaLog.setRequestInTime(new Date());
			idaLog.setSsn(appData.getSsn());
			idaLog.setCronJobId(cronJobId);
			idaLogDao.create(idaLog);
			logId=idaLog.getId();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex){ex.printStackTrace();}
		return logId;
	}
	
	@Override
	public void logResponse(Long id, String responseBody,Boolean isProcessed) {
		try {
			IDALog idaLog = idaLogDao.get(id);
			idaLog.setResponseOutTime(new Date());
			idaLog.setIsProcessed(isProcessed);
			if(isProcessed)
				idaLog.setResponseBody(responseBody);
			else
				idaLog.setError(responseBody);
			idaLogDao.update(idaLog);
			
		} catch (Exception e) {
			System.out.println("Exception in logging response "+e);
			e.printStackTrace();
		}
		
	}
	

}
