package com.lendingpoint.service;

import com.lendingpoint.domain.ApplicationData;

public interface IDALogService {
	public Long logRequest(ApplicationData appData,String cronJobId);
	public void logResponse(Long id, String responseBody,Boolean isProcessed);
}
