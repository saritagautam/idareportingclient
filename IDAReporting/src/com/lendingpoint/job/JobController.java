package com.lendingpoint.job;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lendingpoint.domain.ApplicationData;
import com.lendingpoint.processor.IDARequestProcessor;
import com.lendingpoint.processor.ReportMailSender;
import com.lendingpoint.processor.ReportProcessor;

@Controller
@RequestMapping("/jobController")
public class JobController {
	
	@Autowired
	ReportMailSender reportMailSender;
	
	@Autowired
	ReportProcessor reportProcessor;
	
	@Autowired
	IDARequestProcessor idaRequestProcessor;
	
	Logger logger = LoggerFactory.getLogger(JobController.class);
	
	@RequestMapping(value = "/run", method = RequestMethod.GET)
	@ResponseBody
	public String run(@RequestParam("key") String key) {
		System.out.println("called" + key);
		String responseData = "";
		if(key.equalsIgnoreCase("7caa9a53-d692-49f5-a3ba-13bec8b32fd3")){
		logger.debug("2.Cron job for IDA executed. Current time is :: " + new Date());
		Integer idaResponseSize = 0;
		responseData = null;
		try
		{    
			String fileName = null;
			
			Calendar c = Calendar.getInstance();
			Boolean isMonday = false;
			if(c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
				isMonday = true;
			long startTime = System.currentTimeMillis();
			List<ApplicationData> listapp = reportProcessor.getCreditQualifiedApps(isMonday);
			if(listapp.size()>0){
				idaResponseSize = listapp.size();
				List<Map<String,Object>> idaResponse = idaRequestProcessor.processApplications(listapp);
				if(idaResponse.size()>0)
					fileName = reportProcessor.createExcelReport(idaResponse);
				if(fileName != null)
				reportMailSender.sendEmail(fileName);
			}
			long stopTime   = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			logger.debug("2.Time taken for CronJob is : "+ elapsedTime);
			responseData = "Number of Records processed : "+idaResponseSize.toString();
		}catch(Exception ex){
			logger.error("exception in running Cron Job");
			StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
			responseData = sw.toString();
		}
		
		}else
			responseData = "Security Key does not match";
		return responseData;
	}
}
