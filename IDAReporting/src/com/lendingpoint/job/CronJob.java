package com.lendingpoint.job;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import com.lendingpoint.domain.ApplicationData;
import com.lendingpoint.processor.IDARequestProcessor;
import com.lendingpoint.processor.ReportMailSender;
import com.lendingpoint.processor.ReportProcessor;
@Configuration
@EnableScheduling
@Component
public class CronJob {
	
	@Autowired
	ReportMailSender reportMailSender;
	
	@Autowired
	ReportProcessor reportProcessor;
	
	@Autowired
	IDARequestProcessor idaRequestProcessor;
	
	Logger logger = LoggerFactory.getLogger(CronJob.class);
	
	public void run()
    {
		logger.debug("Cron job for IDA executed. Current time is :: " + new Date());
		try
		{    
			String fileName = null;
			
			Calendar c = Calendar.getInstance();
			Boolean isMonday = false;
			if(c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
				isMonday = true;
			long startTime = System.currentTimeMillis();
			List<ApplicationData> listapp = reportProcessor.getCreditQualifiedApps(isMonday);
			if(listapp.size()>0){
				List<Map<String,Object>> idaResponse = idaRequestProcessor.processApplications(listapp);
				if(idaResponse.size()>0)
					fileName = reportProcessor.createExcelReport(idaResponse);
				if(fileName != null)
				reportMailSender.sendEmail(fileName);
			}
			long stopTime   = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			logger.debug("Time taken for CronJob is : "+ elapsedTime);
		}catch(Exception ex){
			logger.error("exception in running Cron Job");
			StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		}
    }
}
