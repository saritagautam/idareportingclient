package com.lendingpoint.processor;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lendingpoint.domain.ApplicationData;
import com.lendingpoint.service.IDALogService;

public class IDARequestProcessor {
   
	@Value("${ida.request.server.url}")
    private String IDA_REQUEST_SERVER_URL;
	
	@Value("${ida.request.batch.size}")
    private int IDA_REQUEST_BATCH_SIZE;
	
	@Autowired
	IDALogService idaLogService;
	
	Logger logger = LoggerFactory.getLogger(IDARequestProcessor.class);
	
    public List<Map<String,Object>> processApplications(List<ApplicationData> appList) {
    	ObjectMapper mapper = new ObjectMapper();
		String appDataInstring = null;
		String responseString = null;
		ApplicationData app = null;
		UUID uuid = UUID.randomUUID();
		String cronJobId = uuid.toString();
		List<Map<String,Object>> responseList = new ArrayList<Map<String,Object>>();
		Long logId = null;
		int maxarraySize=IDA_REQUEST_BATCH_SIZE;
		int index = 0;
		logger.debug("CronJob Id fro today " +new Date()+" is :"+ cronJobId);
		try 
		{
			List<ApplicationData> subAppList = new ArrayList<ApplicationData>();
			Map<String, String>[] indicatorResponseListData = null;
			
			int noOfSublist = appList.size()/maxarraySize;
			int remainder =appList.size() % maxarraySize;
			if(appList.size() % maxarraySize != 0)
				noOfSublist++;
			for(int i=0;i<noOfSublist;i++){
				if(i==noOfSublist-1)
					subAppList = appList.subList(index, appList.size());
				else
					subAppList = appList.subList(index, index+maxarraySize);
				System.out.println("Array "+i +" "+index +" :: "+(index+maxarraySize));
				index = index+maxarraySize;
				appDataInstring = mapper.writeValueAsString(subAppList);
				responseString = getIDAReponse(appDataInstring);
				if(responseString != null){
					indicatorResponseListData = mapper.readValue(responseString,
							new TypeReference<Map<String, String>[]>() {
							});
					for(Map<String, String> indicatorResponseData : indicatorResponseListData){
						app = mapper.readValue(indicatorResponseData.get("appData"),ApplicationData.class);
						logId = idaLogService.logRequest(app,cronJobId);
						Map<String,Object> indicatorAppDetails = new HashMap<String,Object>();
						
						if(indicatorResponseData.get("error") == null){
							indicatorAppDetails.put("indicatorValues", indicatorResponseData);
							idaLogService.logResponse(logId,mapper.writeValueAsString(indicatorResponseData),true);
						}
						else{
							indicatorAppDetails.put("error", indicatorResponseData.get("error"));
							idaLogService.logResponse(logId,mapper.writeValueAsString(indicatorResponseData.get("error")),false);
						}
						indicatorAppDetails.put("appDetails", app);
						responseList.add(indicatorAppDetails);
					}
				}
			}
			
			
			}catch(Exception ex){
			System.out.println(ex);
			ex.printStackTrace();
		}
		System.out.println(responseList.size());
		return responseList;
    }
    
    private String getIDAReponse(String idaRequest){
    	String responseStr = null;
    	try{
    		Thread.sleep(1000);
    		URL obj = new URL(IDA_REQUEST_SERVER_URL);
    		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    		con.setRequestMethod("POST");
    		con.setRequestProperty("Content-Type", "application/json");
    		con.setRequestProperty("Accept", "text/xml");
    		con.setUseCaches(false);
    		con.setDoInput(true);
    		con.setDoOutput(true);
    		//con.setConnectTimeout(3000000);
    		//con.setReadTimeout(3000000);
    		//Send request
    	    DataOutputStream wr = new DataOutputStream (con.getOutputStream());
    	    wr.writeBytes (idaRequest);
    	    wr.flush ();
    	    wr.close ();
    	    //Get Response	
    		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    		String inputLine;
    		StringBuilder response = new StringBuilder();
     
    		while ((inputLine = in.readLine()) != null) {
    			response.append(inputLine);
    		}
    		in.close();
    		
    		responseStr = response.toString();
    	}catch(Exception ex){
    		StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            logger.error("Exception in connecting to IDA : " +sw.toString());
    	}
    	return  responseStr;
    }
}