package com.lendingpoint.processor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import com.lendingpoint.domain.ApplicationData;

public class ReportProcessor {

	@Value("${database.url}")
    private String DATABASE_URL;
	
	@Value("${database.username}")
    private String DB_USERNAME;
	
	@Value("${database.password}")
    private String DB_PASSWORD;
	
	@Value("${excel.file.relative.path}")
	private String relativePath;
	
	@Value("${excel.file.name}")
	private String filePath;
	
	@Value("${ida.process.last.left.applications}")
	private Boolean PROCESS_OLD_APPS;
	
	private String rootPath = System.getProperty("catalina.home");
	
	Logger logger = LoggerFactory.getLogger(ReportProcessor.class);
		
	public  List<ApplicationData> getCreditQualifiedApps(Boolean isMonday){
		logger.debug("Get CreditQualified apps called");
		List<ApplicationData> listapp = new ArrayList<ApplicationData>();
		List<ApplicationData> oldAppList = new ArrayList<ApplicationData>();
		String queryInterval = "24";
		/*if(isMonday)
			queryInterval = "72";*/
		try
		{
			SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
	        dataSource.setDriver(new com.mysql.jdbc.Driver());
	        
	        dataSource.setUrl(DATABASE_URL);
	        dataSource.setUsername(DB_USERNAME);
	        dataSource.setPassword(DB_PASSWORD); 
	        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
	 
	        String sqlSelect = "SELECT app.Id,"
	        		+ "app.name,"
	        		+ "con.genesis__SSN__c,"
	        		+ "con.title,"
	        		+ "con.firstName,"
	        		+ "con.lastName,"
	        		+ "con.birthdate,"
	        		+ "con.mailingStreet,"
	        		+ "con.mailingCity,"
	        		+ "con.mailingPostalCode,"
	        		+ "con.phone,"
	        		+ "con.email,"
	        		+ "con.mailingState,"
	        		+ "app.createdDate "
	        		+ "FROM genesis__Applications__c app "
	        		+ "Inner Join Contact con on con.ID=app.genesis__Contact__c "
	        		+ "where app.Id in (select parentId from genesis__Applications__History "
	        		+ "where createdDate > DATE_SUB(NOW(), INTERVAL 24 HOUR) "
	        		+ "and newValue='Credit Qualified' and field='genesis__status__c' order by createdDate)";
	        listapp = fetchDataFromDB(sqlSelect,jdbcTemplate);
	        logger.debug("Size of application data from 24 hours : " + listapp.size());
	        if(PROCESS_OLD_APPS){
		        //query to get old applications which were left because of data synching
		        sqlSelect = "SELECT app.Id,"+
	        		"app.name,"+
	        		"con.genesis__SSN__c,"+
	        		"con.title,"+
	        		"con.firstName,"+
	        		"con.lastName,"+
	        		"con.birthdate,"+
	        		"con.mailingStreet,"+
	        		"con.mailingCity,"+
	        		"con.mailingPostalCode,"+
	        		"con.phone,"+
	        		"con.email,"+
	        		"con.mailingState,"+
	        		"app.createdDate "+
	        		"FROM genesis__Applications__c app "+ 
	        		"Inner Join Contact con on con.ID=app.genesis__Contact__c "+ 
	        		"where app.Id in (select parentId from genesis__Applications__History "+ 
	        		"where createdDate > DATE_SUB(NOW(), INTERVAL 48 HOUR) and "+
					"createdDate < DATE_SUB(NOW(), INTERVAL 24 HOUR) "+
	        		"and newValue='Credit Qualified' and field='genesis__status__c' and "+
					"parentId in (select applicationId from ida_log where requestInTime >  DATE_SUB(NOW(), INTERVAL 48 HOUR))order by createdDate)";
		        	        
		        oldAppList = fetchDataFromDB(sqlSelect,jdbcTemplate);
		        logger.debug("Size of application data from last 48 hours which were not processed : " + oldAppList.size());
		        if(oldAppList.size()>0){
		        	for (ApplicationData app :oldAppList){
		        		listapp.add(app);
		        	}
		        }
	        }
		}catch(Exception ex){
			StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            logger.error("Exception in getApplications : " +sw.toString());
		}
		//For creating test responses
		/*int count=1;
        for (ApplicationData aContact : listapp) {
            if(count==1){
            	aContact.setSsn("301001201");
            	aContact.setZip("80001");
            	count++;
            }
            if(count==2){
            	aContact.setSsn("301001208");
            	aContact.setZip("80008");
            	count++;
            }
            if(count==3){
            	aContact.setSsn("301001215");
            	aContact.setZip("80015");
            	count++;
            }else if(count==4){
            	aContact.setSsn("301001211");
            	aContact.setZip("80011");
            	count++;
            }
        }*/
		logger.debug("Application list size : " +listapp.size());
		return listapp;
        
	}
	
	public String createExcelReport(List<Map<String,Object>> appIndicatorList){
		logger.debug("inside createExcelReport");
		String fileName= "";
		
		DateFormat formatterEST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dateObj = null;
		int olnagAttributescount = 95;
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Report");
		HSSFSheet sheet2 = null;
		CellStyle style = workbook.createCellStyle();//Create style
	    Font font = workbook.createFont();//Create font
	    font.setBold(true);
	    style.setFont(font);
	    int columnCount1041 = 6;
		try 
		{
			HSSFRow rowhead = sheet.createRow((short)0);
			rowhead.createCell(0).setCellValue("ApplicationId");
			rowhead.getCell(0).setCellStyle(style);
	        rowhead.createCell(1).setCellValue("App-Name");
	        rowhead.getCell(1).setCellStyle(style);
	        rowhead.createCell(2).setCellValue("FirstName");
	        rowhead.getCell(2).setCellStyle(style);
	        rowhead.createCell(3).setCellValue("LastName");
	        rowhead.getCell(3).setCellStyle(style);
	        rowhead.createCell(4).setCellValue("App-CreatedDate");
	        rowhead.getCell(4).setCellStyle(style);
	        rowhead.createCell(5).setCellValue("App-IDAResponseDate");
	        rowhead.getCell(5).setCellStyle(style);
	        rowhead.createCell(columnCount1041).setCellValue("OLNAG1041");
	        rowhead.getCell(columnCount1041).setCellStyle(style);
	        /*rowhead.createCell(6).setCellValue("SSN");
	        rowhead.getCell(6).setCellStyle(style);
	        rowhead.createCell(7).setCellValue("ZIP");
	        rowhead.getCell(7).setCellStyle(style);*/
	        
	        int columnHeadCount = 7;
	        int startValue = 1000;
	        for(int i=1;i<=olnagAttributescount;i++)        
	        {
	        	if((1000+i) !=1041){
	        		rowhead.createCell(columnHeadCount).setCellValue("OLNAG"+(1000+i));
		        	rowhead.getCell(columnHeadCount).setCellStyle(style);
		        	sheet.autoSizeColumn(i);
		        	columnHeadCount++;
	        	}
	        }
	        rowhead.createCell(columnHeadCount).setCellValue("Errors");
	        rowhead.getCell(columnHeadCount).setCellStyle(style);
	        int errorColumnCount = columnHeadCount;
	        int sheetRowCounter = 1;
	        int sheet2RowCounter = 1;
	        ApplicationData appData = null;
	        Map<String, String> indicatorResponseData = new HashMap<String, String>(); 
	        Boolean isDataOnSecondSheet = false;
	        String createdDate = "";
	        String processedDate="";
	        for(Map<String,Object> mapData : appIndicatorList){
	        	columnHeadCount = 7;
	        	HSSFRow row = sheet.createRow((short)sheetRowCounter);
	        	appData = (ApplicationData) mapData.get("appDetails");
	        	indicatorResponseData = (Map<String, String>) mapData.get("indicatorValues");
	        	row.createCell(0).setCellValue(appData.getAppId());
	            row.createCell(1).setCellValue(appData.getAppName());
	            row.createCell(2).setCellValue(appData.getFirstName());
	            row.createCell(3).setCellValue(appData.getLastName());
	            //set date in EST format
	            if(appData.getCreatedDate()!= null && appData.getCreatedDate() != ""){
	            	try {
						dateObj = formatterUTC.parse(appData.getCreatedDate());
						formatterEST.setTimeZone(TimeZone.getTimeZone("America/New_York")); 
						createdDate = formatterEST.format(dateObj);
		            	row.createCell(4).setCellValue(createdDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	
	            }
	            if(appData.getProcessedDate()!= null && appData.getProcessedDate() != ""){
	            	try {
						dateObj = formatterUTC.parse(appData.getProcessedDate());
						formatterEST.setTimeZone(TimeZone.getTimeZone("America/New_York")); 
						processedDate = formatterEST.format(dateObj);
		            	row.createCell(5).setCellValue(processedDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	
	            }
	            /*row.createCell(6).setCellValue(appData.getSsn());
	            row.createCell(7).setCellValue(appData.getZip());*/
	            Float indicatorValue = null;
	        	if(indicatorResponseData != null){
		            for(int i=1;i<=olnagAttributescount;i++)        
		            {
		            	if((1000+i) !=1041)
		            		row.createCell(columnHeadCount).setCellValue(indicatorResponseData.get("OLNAG"+(1000+i)));
		            	else
		            		row.createCell(columnCount1041).setCellValue(indicatorResponseData.get("OLNAG"+(1000+i)));
	            		if((1000+i)==1041 && !indicatorResponseData.get("OLNAG"+(1000+i)).equalsIgnoreCase("false") && !indicatorResponseData.get("OLNAG"+(1000+i)).equalsIgnoreCase("0") && !indicatorResponseData.get("OLNAG"+(1000+i)).equalsIgnoreCase("-2.0")){
	            			try{
	            				indicatorValue = Float.parseFloat(indicatorResponseData.get("OLNAG"+(1000+i)));
	            			}catch(NumberFormatException ex){
	            				indicatorValue=(float) 0;
	            			}
	            			if(indicatorValue>=3){
		            			if(!isDataOnSecondSheet){
		            				sheet2 = workbook.createSheet("NumLenderApps30Days");
		            				HSSFRow rowheadSheet2 = sheet2.createRow((short)0);
		            				rowheadSheet2.createCell(0).setCellValue("ApplicationId");
		            				rowheadSheet2.getCell(0).setCellStyle(style);
		            				rowheadSheet2.createCell(1).setCellValue("App-Name");
		            				rowheadSheet2.getCell(1).setCellStyle(style);
		            				rowheadSheet2.createCell(2).setCellValue("FirstName");
		            				rowheadSheet2.getCell(2).setCellStyle(style);
		            				rowheadSheet2.createCell(3).setCellValue("LastName");
		            				rowheadSheet2.getCell(3).setCellStyle(style);
		            				rowheadSheet2.createCell(4).setCellValue("App-CreatedDate");
		            				rowheadSheet2.getCell(4).setCellStyle(style);
		            				rowheadSheet2.createCell(5).setCellValue("App-IDAResponseDate");
		            				rowheadSheet2.getCell(5).setCellStyle(style);
		            				rowheadSheet2.createCell(6).setCellValue("OLNG1041");
		            				rowheadSheet2.getCell(6).setCellStyle(style);
		            				rowheadSheet2.createCell(7).setCellValue("OLNG1040");
		            				rowheadSheet2.getCell(7).setCellStyle(style);
		            				rowheadSheet2.createCell(8).setCellValue("OLNG1037");
		            				rowheadSheet2.getCell(8).setCellStyle(style);
		            				rowheadSheet2.createCell(9).setCellValue("OLNG1035");
		            				rowheadSheet2.getCell(9).setCellStyle(style);
		            				isDataOnSecondSheet = true;
		            				            				
		            			}
		            			HSSFRow rowSheet2 = sheet2.createRow((short)sheet2RowCounter);
			            		rowSheet2.createCell(0).setCellValue(appData.getAppId());
			            		rowSheet2.createCell(1).setCellValue(appData.getAppName());
			            		rowSheet2.createCell(2).setCellValue(appData.getFirstName());
			            		rowSheet2.createCell(3).setCellValue(appData.getLastName());
			            		rowSheet2.createCell(4).setCellValue(createdDate);
			            		rowSheet2.createCell(5).setCellValue(processedDate);
			            		rowSheet2.createCell(6).setCellValue(indicatorResponseData.get("OLNAG"+(1000+i)));
			            		rowSheet2.createCell(7).setCellValue(indicatorResponseData.get("OLNAG"+(1040)));
			            		rowSheet2.createCell(8).setCellValue(indicatorResponseData.get("OLNAG"+(1037)));
			            		rowSheet2.createCell(9).setCellValue(indicatorResponseData.get("OLNAG"+(1035)));
			            		
			            		sheet2.autoSizeColumn(0);
	            		        sheet2.autoSizeColumn(1);
	            		        sheet2.autoSizeColumn(2);
	            		        sheet2.autoSizeColumn(3);
	            		        sheet2.autoSizeColumn(4);
	            		        sheet2.autoSizeColumn(5);
	            		        sheet2.autoSizeColumn(6);
	            		        sheet2.autoSizeColumn(7);
	            		        sheet2.autoSizeColumn(8);
	            		        sheet2.autoSizeColumn(9);
	            		        sheet2RowCounter++;
		            		}
	            		}
	            		if((1000+i) !=1041)
	            			columnHeadCount++;
		            }
		            sheetRowCounter++;
		        }else{//error 
		        	row.createCell(errorColumnCount).setCellValue(getErrorMessage((String) mapData.get("error")));
		        	sheetRowCounter++;
		        }
	        }
	        sheet.autoSizeColumn(0);
	        sheet.autoSizeColumn(1);
	        sheet.autoSizeColumn(2);
	        sheet.autoSizeColumn(3);
	       
	        //filePath
	        FileOutputStream fileOut = null;
	       // File directory = new File(String.valueOf(rootPath+this.relativePath));
	       // if(!directory.exists())
	       // 	directory.mkdir();
	        formatterEST = new SimpleDateFormat("yyyyMMdd_HHmm");
			formatterEST.setTimeZone(TimeZone.getTimeZone("EST"));
			String date = formatterEST.format(new Date());//
			fileName = this.relativePath+this.filePath+"_"+date+".xls";
			fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			logger.error("error " + e);
		}catch (IOException e) {
			logger.error("error " + e);
		}   
		return fileName;
	}
	
    private String getErrorMessage(String errorObj){
		String message = "";
		int beginIndex=0;
		int endIndex = 0;
    	if(errorObj.indexOf("api1:Message") != -1){
    		beginIndex = errorObj.indexOf("api1:Message")+13;
    		endIndex = errorObj.indexOf("/api1:Message")-1;
    		message = errorObj.substring(beginIndex, endIndex);
    	}
    	System.out.println("message :::::::: " +message);
    	return message;
    }
    
    private List<ApplicationData> fetchDataFromDB(String sqlSelect,JdbcTemplate jdbcTemplate){
    	
    	List<ApplicationData> listapp = jdbcTemplate.query(sqlSelect, new RowMapper<ApplicationData>()
        {
        	String phone = null;
	        public ApplicationData mapRow(ResultSet result, int rowNum) throws SQLException 
	        {
	            ApplicationData app = new ApplicationData();
            	app.setAppId(result.getString("Id"));
            	app.setAppName(result.getString("name")==null?"":result.getString("name"));
            	app.setSsn(result.getString("genesis__SSN__c")==null?"":result.getString("genesis__SSN__c"));
            	app.setTitle(result.getString("title") ==null?"":result.getString("title"));
            	app.setFirstName(result.getString("firstName") ==null?"":result.getString("firstName"));
            	app.setLastName(result.getString("lastName")==null?"":result.getString("lastName"));
            	if(result.getString("birthdate") != null){
            		 SimpleDateFormat sf = new  SimpleDateFormat("yyyy-MM-dd");
            		 String date = null;
            		 date =sf.format(result.getDate("birthdate"));
						
            		app.setDob(date);
            	}else
            	app.setDob("");	
            	app.setAddress(result.getString("mailingStreet")==null?"":result.getString("mailingStreet"));
            	app.setCity(result.getString("mailingCity")==null?"":result.getString("mailingCity"));
            	app.setState(result.getString("mailingState")==null?"":result.getString("mailingState"));
            	app.setZip(result.getString("mailingPostalCode")==null?"":result.getString("mailingPostalCode"));
            	if(result.getString("phone")!=null){
            		 phone = result.getString("phone");
            		 phone =phone.replaceAll("[()\\s-]+", "");
            		 app.setMobilePhone(phone);
            	}else
            		app.setMobilePhone("");
            	app.setEmail(result.getString("email")==null?"":result.getString("email"));
            	app.setAddress(result.getString("mailingstreet")==null?"":result.getString("mailingstreet"));
            	app.setCreatedDate(result.getString("createdDate")==null?"":result.getString("createdDate"));
            	return app;
	        }	             
       });
    	return listapp;
    }
}
