package com.lendingpoint.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class ReportMailSender {

	@Autowired
	JavaMailSender mailSender;
	
	@Value("${mail.from}")
    private String MAIL_FROM;
	
	@Value("${mail.to}")
	private String MAIL_TO;
	
	@Value("${mail.bcc}")
	private String MAIL_BCC;
	
	Logger logger = LoggerFactory.getLogger(ReportMailSender.class);
	
	public void sendEmail(String fileName){
		try {
			logger.debug("in mail sender");
			
			//convert UTC to EST date
			Date date = new Date();
			DateFormat formatterEST = new SimpleDateFormat("yyyyMMdd");
			formatterEST.setTimeZone(TimeZone.getTimeZone("EST")); // UTC timezone
			String estDate = formatterEST.format(date);
			
			String[] sendBcc = null;
			if(this.MAIL_BCC.length()>1)
				sendBcc=this.MAIL_BCC.split(",");
            String body = "Hi Team,<br/><br/>Please find attached IDA Analytics OLN Attribute report.<br/><br/>Thanks,<br/>IT Team";
            MimeMessage msg = mailSender.createMimeMessage();
            
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);
            helper.setFrom(this.MAIL_FROM);
            helper.setTo(this.MAIL_TO);
            if(sendBcc != null)
            	helper.setBcc(sendBcc);
            
            FileSystemResource file = new FileSystemResource(fileName);
    		//helper.addAttachment(file.getFilename(), new ByteArrayResource(content));
    		helper.addAttachment(file.getFilename(), file);
            msg.setSubject("IDA OLN Response Data "+estDate);
            //helper.setText(body, true);;
            
            //set body
            helper.setText(body, true);
           
			
            mailSender.send(msg);
        } catch (MailException ex) {
            logger.error("MailException : "+ex);
        } catch (MessagingException e) {
        	logger.error("MessagingException : "+e);
		}catch(Exception ex){
			logger.error("Exception in sending email : "+ex);
		}
		logger.debug("Mail sending done");
	}
}
