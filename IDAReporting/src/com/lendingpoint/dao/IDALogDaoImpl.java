package com.lendingpoint.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.lendingpoint.domain.IDALog;

@Repository("idaLogDao")
public class IDALogDaoImpl implements IDALogDao{

	@Autowired
	@Qualifier(value="sessionFactory")
	private SessionFactory sessionFactory;
	@Override
	public void create(IDALog idaLog) {
		sessionFactory.getCurrentSession().save(idaLog);		
	}

	@Override
	public void update(IDALog idaLog) {
		sessionFactory.getCurrentSession().update(idaLog);		
	}
	
	@Override
	public IDALog get(Long id) {
		return (IDALog) sessionFactory.getCurrentSession().get(IDALog.class, id);
	}

}
