package com.lendingpoint.dao;

import com.lendingpoint.domain.IDALog;

public interface IDALogDao {
	public void create(IDALog idaLog);
	public void update(IDALog idaLog);
	public IDALog get(Long id);
}
