package com.lendingpoint.domain;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="ida_log")
public class IDALog {
	@Id @GeneratedValue
	Long id;
	
	String applicationId;
	String applicationName;
	@Column(nullable= true)
	String ssn;
	@Column(nullable= true)
	String firstName;
	@Column(nullable= true)
	String lastName;
	@Column(nullable= true)
	@Lob
	String requestBody;
	@Column(nullable= true)
	@Lob
	String responseBody;
	@Column(nullable= true)
	@Lob
	String error;
	
	@Column(nullable= true)
	Date requestInTime;
	@Column(nullable= true)
	Date responseOutTime;
	
	Boolean isProcessed;
	
	@Column(nullable= true)
	String cronJobId;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Date getRequestInTime() {
		return requestInTime;
	}

	public void setRequestInTime(Date requestInTime) {
		this.requestInTime = requestInTime;
	}

	public Date getResponseOutTime() {
		return responseOutTime;
	}

	public void setResponseOutTime(Date responseOutTime) {
		this.responseOutTime = responseOutTime;
	}

	public Boolean getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public String getCronJobId() {
		return cronJobId;
	}

	public void setCronJobId(String cronJobId) {
		this.cronJobId = cronJobId;
	}
}
